# BBR Plus一键安装脚本（四合一）

#### 介绍
BBR Plus一键安装脚本（四合一）。除了BBR Plus外，还另外集成有原版BBR一键安装、魔改BBR一键安装、锐速（lotServer）一键安装，为四合一版本，四个版本可以切换使用。

适用架构：KVM / Xen，不支持OpenVZ（OVZ）。
适用系统：CentOS 7、Debian 8、Debian 9、Ubuntu 16.04、Ubuntu 18.04。

#### 安装教程

BBR Plus一键安装脚本使用：
1.FinalShell,Xshell,Putty连接服务器，运行如下脚本：


```
yum install wget -y && wget -O tcp.sh https://gitee.com/suiyuansoft/Linux-NetSpeed/raw/master/tcp.sh && bash tcp.sh
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request